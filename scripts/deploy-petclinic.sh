#!/bin/bash

NEXUS_USERNAME=$1
NEXUS_PASSWORD=$2
CONTAINER_REGISTRY=$3
APP_NAME=$4
MYSQL_ENDPOINT_NAME='${MYSQL_ENDPOINT_NAME}'

cat > deploy-petclinic-files.yaml <<EOF
write_files:
  - path: /tmp/docker-compose.yaml
    content: |
      version: "2.2"
      services:
        petclinic:
          image: ${CONTAINER_REGISTRY}/${APP_NAME}
          container_name: petclinic
          ports:
            - 8080:8080
          environment:
            - MYSQL_URL=jdbc:mysql://${MYSQL_ENDPOINT_NAME}/petclinic
runcmd:
  - [/bin/bash, -c, "export MYSQL_ENDPOINT_NAME=${MYSQL_ENDPOINT_NAME}"]
  - [/bin/bash, -c, "echo '$NEXUS_PASSWORD' | docker login -u $NEXUS_USERNAME --password-stdin ${CONTAINER_REGISTRY}"]
  - [/bin/bash, -c, "docker-compose -f /tmp/docker-compose.yaml up --detach"]
EOF